package com.example.plantgrowapp_final;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class croplist extends ArrayAdapter<crophelper> {
    String mycroptype;
    String myacreage;
    Activity con;
    private List<crophelper> alist;

    public croplist(Activity context, List<crophelper> mlist) {
        super(context, R.layout.datadisplay, mlist);
        con = context;
        alist = mlist;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = con.getLayoutInflater();
        View litem = inflater.inflate(R.layout.datadisplay, null, true);
        TextView cpname = (TextView) litem.findViewById(R.id.cropnames);
        TextView acre = (TextView) litem.findViewById(R.id.acresss);
        TextView yield = (TextView) litem.findViewById(R.id.tgyields);
        TextView fertil = (TextView) litem.findViewById(R.id.fertzz);
        TextView cprice = (TextView) litem.findViewById(R.id.cprices);
        ImageView iv = (ImageView) litem.findViewById(R.id.imgvv);
        Button bt=(Button)litem.findViewById(R.id.btndetails);
        crophelper ah = alist.get(position);
        //population the textviews with data
        cpname.setText(ah.getCropname());
        mycroptype=ah.getCropname();
        acre.setText(ah.getAcreage());
        myacreage=ah.getAcreage();
        yield.setText(ah.getYield());
        fertil.setText(ah.getFert());
        cprice.setText(ah.getPrice());
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it=new Intent(con,updateanddelete.class);
                it.putExtra("ctype",mycroptype);
                it.putExtra("cacres",myacreage);
                con.startActivity(it);
                //Toast.makeText(con, "Trial Successful", Toast.LENGTH_SHORT).show();
            }
        });
        return litem;
    }
}