package com.example.plantgrowapp_final;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class updateanddelete extends AppCompatActivity {
    Bundle extras;
    String newString;
    EditText ed;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updateanddelete);
        extras = getIntent().getExtras();

        if(extras == null)
            {
                newString= null;
            }
        else
            {/* fetching the string passed with intent using ‘extras’*/
               newString= extras.getString("cacres");
            }
        ed=(EditText)findViewById(R.id.upacres);
        ed.setText(newString);
    }
}
