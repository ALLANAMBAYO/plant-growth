package com.example.plantgrowapp_final;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.*;
import com.google.firebase.auth.FirebaseUser;

public class registration extends AppCompatActivity {
    EditText tx1,tx2;
    Button bt1 ,blogin;
    String mymail;
    String password;
    Intent it;
   private FirebaseAuth fb;//=FirebaseAuth.getInstance();
   // Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
       //Creation of an instance of firebase authentication
       fb= FirebaseAuth.getInstance();

        if (fb.getCurrentUser() != null) {
            startActivity(new Intent(registration.this, optionspanel.class));
            finish();
        }
        tx1=(EditText)findViewById(R.id.remail);
        tx2=(EditText)findViewById(R.id.rpass);
        bt1=(Button)findViewById(R.id.ureg);
        blogin=(Button)findViewById(R.id.lg);
        it=new Intent(this,login.class);

        //this login button starts the login activity where the users get prompted to login into the system
        blogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(it);
            }
        });

        //this is the registration button which the users hit to have their details registered into our system
        bt1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
        //this is a call being made to the user creation mmethod that facilitates the registration of the new system users
                userCreation();
            }
        });
    }
  //this method fires upon start of this activity to check if their is already a user that is currently logged in
  /*  @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = fb.getCurrentUser();
        //updateUI(currentUser);
            }*/
//this method facilitates user creation using email and password authentication
    public void userCreation()
    {
        //here we trim the input to remove whitespaces
        mymail=tx1.getText().toString().trim();
        password=tx2.getText().toString().trim();

        //here we perform checks on the input to find out if they are empty or wholly filled up
          if(TextUtils.isEmpty(mymail))
            {
                Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
                return;
            }

        if(TextUtils.isEmpty(password))
            {
                Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
                return;
            }
        if (password.length() <8) {
            Toast.makeText(getApplicationContext(), "Password too short, enter minimum 8 characters with numbers included too!", Toast.LENGTH_SHORT).show();
            return;
        }
     //this is the method that now creates the new user account with firebase
         fb.createUserWithEmailAndPassword(mymail, password)
                .addOnCompleteListener(registration.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(registration.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                        //progressBar.setVisibility(View.GONE);
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(registration.this, "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            startActivity(new Intent(registration.this, optionspanel.class));
                            finish();
                        }
                    }
                });
    }

}
