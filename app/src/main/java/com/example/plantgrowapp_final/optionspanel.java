package com.example.plantgrowapp_final;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class optionspanel extends AppCompatActivity {
    ImageView test;
    Button pics;
    TextView tvw;
    Button reg;
    Button rect;
    Button vd;
    Intent it1;
    Intent it2,it3,it4,it5;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionspanel);

        auth = FirebaseAuth.getInstance();
        pics=(Button)findViewById(R.id.pict);
        reg=(Button)findViewById(R.id.rgst);
        rect=(Button) findViewById(R.id.recs);
        tvw=(TextView)findViewById(R.id.logout);
        vd=(Button)findViewById(R.id.viewdata);
        test=(ImageView)findViewById(R.id.thelogo);

        //intents
        it1=new Intent(this,cropimage.class);
        it2=new Intent(this,cropregistration.class);
        it3=new Intent(this,registration.class);
        it4=new Intent(this,cropdata.class);
        it5 = new Intent(this, recommendations.class);
        pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(it2);
            }
        });

        //test.setOnClickListener(new View.OnClickListener() {
          //  @Override
            //public void onClick(View v) {
                //FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
                //Toast.makeText(optionspanel.this, currentFirebaseUser.getEmail() + "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();

                //String mdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                //Toast.makeText(optionspanel.this, mdate, Toast.LENGTH_SHORT).show();

              //  GetCurrentLocation mGpsLocationTracker = new GetCurrentLocation(getApplication());

                /**
                 * Set GPS Location fetched address
                 */
                /*if (mGpsLocationTracker.canGetLocation())
                {
                    String latitude = ""+mGpsLocationTracker.getLatitude();
                    String longitude = ""+mGpsLocationTracker.getLongitude();
                    Toast.makeText(optionspanel.this,"Latitude: " +latitude+" Longitude: "+longitude+" Location: "+mGpsLocationTracker.getLocation(), Toast.LENGTH_SHORT).show();
                    //Log.i(TAG, String.format("latitude: %s", latitude));
                    //Log.i(TAG, String.format("longitude: %s", longitude));

                }
                else
                {
                    mGpsLocationTracker.showSettingsAlert();
                }
            }
        });*/

        vd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(it4);
            }
        });
        rect.setOnClickListener(new View.OnClickListener()
     {
         @Override
         public void onClick(View v) {
            startActivity(it5);
         }
     });
     reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(it2);
            }
        });
     tvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                startActivity(it3);
            }
        });

    }
}
