package com.example.plantgrowapp_final;

public class cropsdb {

    String cropname;
    String cropyield;
    String cropprice;
    String acreage;
    String fert;

    public cropsdb()
    {

    }

    public cropsdb(String crop, String acres, String yield,String price,String fert) {
        this.cropname = crop;
        this.cropyield = yield;
        this.cropprice = price;
        this.acreage=acres;
        this.fert=fert;
    }

    public String getCropname()
    {
        return cropname;
    }



    public String getCropyield() {
        return cropyield;
    }

    public String getCropprice() {
        return cropprice;
    }

     public String getAcreage() {
        return acreage;
    }

    public String getFert() {
        return fert;
    }


}
