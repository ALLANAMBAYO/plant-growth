package com.example.plantgrowapp_final;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

public class cropregistration extends AppCompatActivity {
     Button bt,mycamera;
     Intent t;
     EditText tx1,tx2,tx3,tx4;
     Spinner sp;
     DatabaseReference dbref= FirebaseDatabase.getInstance().getReference("crops");
     ImageView imggView;
//my new fields
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;
    private static final int CAMERA_REQUEST = 1888;
    Bitmap bitmap;
    ByteArrayOutputStream baos;
    byte[] dataBAOS;
    //Firebase
    FirebaseStorage storage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cropregistration);
        //firebase pointers initialization
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        //other fields initialization
        bt=(Button)findViewById(R.id.nxt);
        mycamera=(Button)findViewById(R.id.cam);
        tx1=(EditText)findViewById(R.id.acres);
        tx2=(EditText)findViewById(R.id.yield);
        tx3=(EditText)findViewById(R.id.price);
        tx4=(EditText)findViewById(R.id.fert);
        sp=(Spinner)findViewById(R.id.crop);
        imggView=(ImageView)findViewById(R.id.mypic);
       // t=new Intent(this,plantnutrients.class);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // startActivity(t);
                //addCrop();
                uploadCrop();
            }
        });

        mycamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                takePic();
            }
        });
    }
    private void takePic()
    {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }
    //start
    //this method starts allows for the display of the selected image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        bitmap = (Bitmap) data.getExtras().get("data");
        imggView.setImageBitmap(bitmap);
        //changed here
        baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        dataBAOS = baos.toByteArray();
    }

    //the file upload method
    private void uploadCrop()
    {
        //Toast.makeText(cropregistration.this, "Testing uploads ", Toast.LENGTH_SHORT).show();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();


       StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
        ref.putBytes(dataBAOS)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.dismiss();
                        imageuploadtestingHelper ih=new imageuploadtestingHelper("Test Caption",taskSnapshot.getUploadSessionUri().toString());
                        String upid=dbref.push().getKey();
                        dbref.child(upid).setValue(ih);
                        Toast.makeText(cropregistration.this, "Uploaded", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(cropregistration.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage("Uploaded "+(int)progress+"%");
                    }
                });
    }
    //end of new code


    private void addCrop()
    {
        String cropname=sp.getSelectedItem().toString();
        String acreage=tx1.getText().toString().trim();
        String yields=tx2.getText().toString().trim();
        String prices=tx3.getText().toString().trim();
        String fertz=tx4.getText().toString().trim();
        if(TextUtils.isEmpty(acreage))
            {
            Toast.makeText(this, "Please Input the Acreage", Toast.LENGTH_SHORT).show();
            }
        if(TextUtils.isEmpty(yields))
            {
            Toast.makeText(this, "Please Input the Yields", Toast.LENGTH_SHORT).show();
            }
        if(TextUtils.isEmpty(prices))
        {
            Toast.makeText(this, "Please Input the Price", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.isEmpty(fertz))
        {
            Toast.makeText(this, "Please Input the Fertilizer", Toast.LENGTH_SHORT).show();
        }
            String id=dbref.push().getKey();
            cropsdb ar=new cropsdb(cropname,acreage,yields,prices,fertz);
            dbref.child(id).setValue(ar);
            Toast.makeText(this, "Crop Registered Successfully", Toast.LENGTH_SHORT).show();

    }

}
