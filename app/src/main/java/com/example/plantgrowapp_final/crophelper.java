package com.example.plantgrowapp_final;

public class crophelper
{
    String cropname;
    String acreage;
    String price;
    String yield;
    String fert;

    public  crophelper()
         {

        }


    public crophelper(String cropname1, String acreage1,String price1, String yield1,String fert1)
        {
            this.cropname = cropname1;
            this.acreage = acreage1;
            this.price = price1;
            this.yield=yield1;
            this.fert=fert1;
        }

    public String getCropname()
        {
            return cropname;
        }

    public String getAcreage()
        {
            return acreage;
        }

    public String getPrice()
        {
            return price;
        }

    public String getYield() {
        return yield;
    }

    public String getFert() {
        return fert;
    }
}
