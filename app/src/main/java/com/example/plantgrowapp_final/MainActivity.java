package com.example.plantgrowapp_final;

import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
   private ProgressBar mypb;
   private int progStatus=0;
   private Handler h=new Handler();
   Intent it;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initializing the progress bar variables created above for the applications startup
        mypb=(ProgressBar)findViewById(R.id.pb);
       // mypb.setBackgroundColor(000);
         it=new Intent(this,registration.class);
        new Thread(new Runnable() {
            @Override
            public void run() {
                //this is the loop that increments the progress bar  values
                while(progStatus<100)
                {
                    progStatus++;//this increments the progress status at each iteration instance
                    SystemClock.sleep(50);//this waits for each 50 milliseconds before updating the progress bar
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            mypb.setProgress(progStatus);//this sets the progess bar new value
                        }
                    });
                }

                h.post(new Runnable() {
                    @Override
                    public void run() {
                        //this displays a message upon the progressbar loading completion
                        //Toast.makeText(MainActivity.this, "Loading Successful", Toast.LENGTH_LONG).show();
                        startActivity(it);
                    }
                });
            }
        }).start();
    }
}
